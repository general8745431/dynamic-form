import React from "react";

export interface Field {
  fieldName: string;
  value: string;
  type: string;
  options?: string[];
}
const apiUrl = process.env.REACT_APP_API_URL;

type FormHookReturnType = [Field[], boolean, (e: React.ChangeEvent<HTMLInputElement | { name: string; value: unknown }>) => void];

const useForm = (): FormHookReturnType => {
  const [formData, setFormData] = React.useState<Field[]>([]);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    if(apiUrl){
      fetch(apiUrl)
        .then(r => r.json())
        .then(r => {
          setFormData(r.data);
          setLoading(false);
        })
        .catch(e => {
          console.error(e.message);
          setLoading(false);
        });
    }
  }, [apiUrl]);

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement | { name: string; value: unknown }>) => {
    const { name, value } = e.target as HTMLInputElement;
    setFormData((prevFormData) =>
      prevFormData.map((field) => (field.fieldName === name ? { ...field, value: value as string } : field))
    );
  };

  return [formData, loading, handleInputChange];
};

export default useForm;
