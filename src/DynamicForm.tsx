import React from 'react';
import useForm, { Field } from './custom_hook/useForm';
import {
  TextField,
  Box,
  Button,
  Container,
  FormControl,
  InputLabel,
  Select,
  CircularProgress,
  MenuItem,
  SelectChangeEvent,
} from '@mui/material';

const apiUrl = process.env.REACT_APP_API_URL;

const FormLoader: React.FC = () => {
  return (
    <Box display="flex" justifyContent="center" alignItems="center" height="100vh">
      <CircularProgress />
    </Box>
  );
};

const DynamicForm: React.FC = () => {
  const [response, setResponse] = React.useState<string | Record<string, unknown>>('');
  const [formData, loading, handleInputChange] = useForm();
  const [display, setDisplay] = React.useState(false)

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Process form data, submit to server.
    const data = new FormData(e.currentTarget);
    const obj: Record<string, unknown> = {};
    data.forEach((v, k) => (obj[k] = v));
    setDisplay(true)
    if(apiUrl){
    fetch(apiUrl, {
      method: 'POST',
      body: JSON.stringify(obj),
      headers: { 'Content-Type': 'application/json' },
    })
      .then((r) => r.json())
      .then((r) => {
        setDisplay(false)
        setResponse(r)
      })
      .catch((e) =>{
        setDisplay(false) 
        console.error(e.message)});}
  };

  const handleSelectChange = (e: SelectChangeEvent<any>) => {
    const { name } = e.target;
    if (name) {
      handleInputChange(e as React.ChangeEvent<HTMLInputElement>);
    }
  };

  try {
    return (
      <Container maxWidth="sm">
        <h1>Dynamic Form</h1>
        {loading ? (
          <FormLoader />
        ) : (
          <form onSubmit={handleSubmit}>
            {formData.map((field: Field) =>
              field.type === 'select' ? (
                <FormControl fullWidth margin="normal" key={field.fieldName}>
                  <InputLabel>{field.fieldName}</InputLabel>
                  <Select
                    name={field.fieldName}
                    value={field.value}
                    onChange={handleSelectChange} // Updated the event handler here
                  >
                    {field.options?.map((option: any) => (
                      <MenuItem key={option} value={option}>
                        {option}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              ) : (
                <TextField
                  key={field.fieldName}
                  label={field.fieldName}
                  name={field.fieldName}
                  value={field.value}
                  type={field.type}
                  multiline={field.type === 'multiline'}
                  onChange={handleInputChange}
                  fullWidth
                  margin="normal"
                />
              )
            )}
            <Button type="submit" variant="contained" color="primary" disabled={display}>
              Submit
            </Button>
          </form>
        )}
        <p>{JSON.stringify(response)}</p>
      </Container>
    );
  } catch (e) {
    return <p> :( </p>;
  }
};

export default DynamicForm;
