# Requirements
- npm or yarn
- mui 
- react

# create application
    `git clone https://gitlab.com/general8745431/dynamic-form`
    `cd dynamic-form`

# install app dependency
    `npm install @mui/material @emotion/react @emotion/styled`

# start application
    `npm start`